export interface MetricInfo {
    connectCount: number
    topicCount: number
    subscriberCount:number
}